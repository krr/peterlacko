from pandas import *
from bisect import *
from collections import defaultdict
from functools import reduce, cmp_to_key

WILDCARD_SYMBOL = "--"

# Here the data structures used for certain concepts will be explained (note the explanation might repeat in further
# comment sections).

# Element (this is an element of the lattice, or an element of the levels of the lattice):
# An element of the lattice is a collection of two items: (1) a list of attributes and (2) a set-pattern tuple. This is
# represented by a tuple of 2 elements: the first element is a tuple of the attributes, the second element is a tuple
# that is the set-pattern tuple.

# level (of the lattice):
# this is represented by a dictionary: the keys are (sorted) tuples of attributes. The values are a list of the
# set-pattern tuples. So each element of the level of the lattice can be reconstructed by combining each key with each
# item in its value list. Furthermore, the algorithm will keep every list of set-pattern tuples sorted from most general
# to least general.

# note: read the paper "Discovering Conditional Functional Dependencies" to see the definition used for creating
# partitions based on elements.

# partition (of the data/indices of tuples, partitioned according to a certain element):
# this is represented by a set of frozensets. The inner frozensets are always a collection of ints. Each
# element of the (outer) set is an equivalence class of the partition. The ints of the frozenset indicate the indices
# of the tuples belonging to that equivalence class. Furthermore, only the equivalence classes that match the
# set-pattern tuple (of the partitioning) are kept. All other equivalence classes are singletons and can be derived by
# the ints in the range of indices that do not appear in a frozenset. In the case of a partitioning according to an
# element with only wildcards in its set-pattern tuple, every item matches the set-pattern tuple. To reduce memory
# requirements, the singleton equivalence classes are dropped, because they can still be derived as mentioned before.

# partitions (all partitions of a level of the lattice):
# this is represented as a dictionary with elements as keys and the corresponding partition (see above) as values.
# note: sometimes the term partitions_keys is used, this refers to the elements of the lattice (keys of the partitions
# dictionary).

# Cplus set (of an element of the lattice):
# this is represented as dictionaries nested in a dictionary. The outer dictionary has elements of the current level as
# keys. It's values is an  'inner' dictionary as follows: the inner dictionary has the same structure as the dictionary
# for levels. It represents all the elements belonging to the Cplus(outer_elem), if outer_elem is the key of the outer
# dictionary. Note that the inner dictionary's keys will always be attribute tuples of length one, due to the definition
# of cplus (the values will also be lists of (set-pattern) tuples, where the tuples are length one)



# A class to represent CSD's, with an overridden method to convert to string
# The first 2 input parameters are lists of strings, the last 2 input parameters are assumed to be lists of "sets of
# strings" or "the wildcard symbol" (outer lists can be singletons or empty lists, inner sets should not be empty)
class CSD:

    def __init__(self, lhs_attributes, rhs_attributes, lhs_pattern, rhs_pattern):
        self.lhs_attributes = lhs_attributes
        self.rhs_attributes = rhs_attributes
        lhs_set_pattern = list(lhs_pattern)
        for i in range(len(lhs_set_pattern)):
            if lhs_set_pattern[i] != WILDCARD_SYMBOL:
                if not isinstance(lhs_set_pattern[i],set) and not isinstance(lhs_set_pattern[i],frozenset):
                    lhs_set_pattern[i] = {lhs_set_pattern[i]}
                elif isinstance(lhs_set_pattern[i], frozenset):
                    lhs_set_pattern[i] = set(lhs_set_pattern[i])
        rhs_set_pattern = list(rhs_pattern)
        for i in range(len(rhs_set_pattern)):
            if rhs_set_pattern[i] != WILDCARD_SYMBOL:
                if not isinstance(rhs_set_pattern[i],set) and not isinstance(rhs_set_pattern[i],frozenset):
                    rhs_set_pattern[i] = {rhs_set_pattern[i]}
                elif isinstance(rhs_set_pattern[i], frozenset):
                    rhs_set_pattern[i] = set(rhs_set_pattern[i])
        self.lhs_pattern = lhs_set_pattern
        self.rhs_pattern = rhs_set_pattern

    def __str__(self):
        return str(self.lhs_attributes) + " ~> " + str(self.rhs_attributes) + ", (" + str(self.lhs_pattern)[1:-1] + \
            " || " + str(self.rhs_pattern)[1:-1] + ")"

    # this function assumes both self and second_csd hold in some relation, and that the rhs exists of only one
    # attribute
    def minimal_compared_to(self, second_csd):
        if len(self.lhs_attributes) > len(second_csd.lhs_attributes):
            return False
        elif self.rhs_attributes != second_csd.rhs_attributes:
            return False
        elif self.lhs_attributes == second_csd.lhs_attributes:
            if sp_tuples_match(tuple(second_csd.lhs_pattern), tuple(self.lhs_pattern)) and \
                    sp_tuples_match(tuple(self.rhs_pattern), tuple(second_csd.rhs_pattern)):
                return True
            else:
                return False
        elif len(self.lhs_attributes) < len(second_csd.lhs_attributes):
            for attr in self.lhs_attributes:
                if attr not in second_csd.lhs_attributes:
                    return False
            attr_diff = set(second_csd.lhs_attributes) - set(self.lhs_attributes)
            second_lhs_pattern = list(second_csd.lhs_pattern)
            second_lhs_attrs = list(second_csd.lhs_attributes)
            for attr in attr_diff:
                del second_lhs_pattern[second_lhs_attrs.index(attr)]
                del second_lhs_attrs[second_lhs_attrs.index(attr)]
            if sp_tuples_match(tuple(second_lhs_pattern), tuple(self.lhs_pattern)) and \
                    sp_tuples_match(tuple(self.rhs_pattern), tuple(second_csd.rhs_pattern)):
                return True
            else:
                return False
        else:
            return False

    def to_fol(self):
        pass #this would require more info (eg relation names)

# the input csd string can only be of the form created by transforming a CSD object into a string (eg what is output by
# csdtaneFinal.py)
def create_csd_from_csd_string(csd_string):
    open_brackets = 0
    close_bracket_index = 0
    for i in range(len(csd_string)):
        if csd_string[i] == "[":
            open_brackets += 1
        elif csd_string[i] == "]":
            open_brackets -= 1
            close_bracket_index = i
        if open_brackets == 0:
            break
    lhs_attrs_string = csd_string[1:close_bracket_index]
    lhs_attrs = lhs_attrs_string.split(", ")
    lhs_attrs = [s[1:-1] for s in lhs_attrs if len(s) >= 2]
    open_brackets = 0
    open_bracket_index = close_bracket_index + 5
    for i in range(open_bracket_index, len(csd_string)):
        if csd_string[i] == "[":
            open_brackets += 1
        elif csd_string[i] == "]":
            open_brackets -= 1
            close_bracket_index = i
        if open_brackets == 0:
            break
    rhs_attrs_string = csd_string[open_bracket_index+1:close_bracket_index]
    rhs_attrs = rhs_attrs_string.split(", ")
    rhs_attrs = [s[1:-1] for s in rhs_attrs if len(s) >= 2]
    set_pattern_tuple_string = csd_string[close_bracket_index+4:-1]
    set_pattern_tuple_sides = set_pattern_tuple_string.split(" || ")
    set_pattern_tuple_lhs_string = set_pattern_tuple_sides[0]
    set_pattern_tuple_rhs_string = set_pattern_tuple_sides[1]
    set_pattern_tuple_lhs = []
    open_brackets = 0
    prev_i = 0
    for i in range(len(set_pattern_tuple_lhs_string)):
        if set_pattern_tuple_lhs_string[i] == "{":
            open_brackets += 1
        elif set_pattern_tuple_lhs_string[i] == "}":
            open_brackets -= 1
        if open_brackets == 0:
            if (set_pattern_tuple_lhs_string[i] == ",") and (set_pattern_tuple_lhs_string[i+1] == " "):
                set_pattern_tuple_lhs.append(set_pattern_tuple_lhs_string[prev_i+1:i-1])
                prev_i = i+2
    set_pattern_tuple_lhs.append(set_pattern_tuple_lhs_string[prev_i+1:-1])
    set_pattern_tuple_lhs = [s for s in set_pattern_tuple_lhs if len(s) > 0]
    set_pattern_tuple_rhs = []
    open_brackets = 0
    prev_i = 0
    for i in range(len(set_pattern_tuple_rhs_string)):
        if set_pattern_tuple_rhs_string[i] == "{":
            open_brackets += 1
        elif set_pattern_tuple_rhs_string[i] == "}":
            open_brackets -= 1
        if open_brackets == 0:
            if (set_pattern_tuple_rhs_string[i] == ",") and (set_pattern_tuple_rhs_string[i+1] == " "):
                set_pattern_tuple_rhs.append(set_pattern_tuple_rhs_string[prev_i+1:i-1])
                prev_i = i+2
    set_pattern_tuple_rhs.append(set_pattern_tuple_rhs_string[prev_i+1:-1])
    set_pattern_tuple_rhs = [s for s in set_pattern_tuple_rhs if len(s) > 0]
    for i in range(len(set_pattern_tuple_lhs)):
        if set_pattern_tuple_lhs[i] == WILDCARD_SYMBOL:
            continue
        else:
            temp_li = set_pattern_tuple_lhs[i].split(", ")
            temp_li = [s[1:-1] for s in temp_li if len(s) >= 2]
            set_pattern_tuple_lhs[i] = set(temp_li)
    for i in range(len(set_pattern_tuple_rhs)):
        if set_pattern_tuple_rhs[i] == WILDCARD_SYMBOL:
            continue
        else:
            temp_li = set_pattern_tuple_rhs[i].split(", ")
            temp_li = [s[1:-1] for s in temp_li if len(s) >= 2]
            set_pattern_tuple_rhs[i] = set(temp_li)
    return CSD(lhs_attrs,rhs_attrs,set_pattern_tuple_lhs,set_pattern_tuple_rhs)

# the input cfd string can only be of the form created by transforming a CFD object into a string
def create_csd_from_cfd_string(cfd_string):
    open_brackets = 0
    close_bracket_index = 0
    for i in range(len(cfd_string)):
        if cfd_string[i] == "[":
            open_brackets += 1
        elif cfd_string[i] == "]":
            open_brackets -= 1
            close_bracket_index = i
        if open_brackets == 0:
            break
    lhs_attrs_string = cfd_string[1:close_bracket_index]
    lhs_attrs = lhs_attrs_string.split(", ")
    lhs_attrs = [s[1:-1] for s in lhs_attrs if len(s) >= 2]
    open_brackets = 0
    open_bracket_index = close_bracket_index + 5
    for i in range(open_bracket_index, len(cfd_string)):
        if cfd_string[i] == "[":
            open_brackets += 1
        elif cfd_string[i] == "]":
            open_brackets -= 1
            close_bracket_index = i
        if open_brackets == 0:
            break
    rhs_attrs_string = cfd_string[open_bracket_index+1:close_bracket_index]
    rhs_attrs = rhs_attrs_string.split(", ")
    rhs_attrs = [s[1:-1] for s in rhs_attrs if len(s) >= 2]
    pattern_tuple_string = cfd_string[close_bracket_index+4:-1]
    pattern_tuple_sides = pattern_tuple_string.split(" || ")
    pattern_tuple_lhs_string = pattern_tuple_sides[0]
    pattern_tuple_rhs_string = pattern_tuple_sides[1]
    set_pattern_tuple_lhs = pattern_tuple_lhs_string.split(", ")
    set_pattern_tuple_lhs = [s[1:-1] for s in set_pattern_tuple_lhs if len(s) >= 2]
    set_pattern_tuple_rhs = pattern_tuple_rhs_string.split(", ")
    set_pattern_tuple_rhs = [s[1:-1] for s in set_pattern_tuple_rhs if len(s) >= 2]
    return CSD(lhs_attrs,rhs_attrs,set_pattern_tuple_lhs,set_pattern_tuple_rhs)

# checks whether the csds in the first file cover the csds in the second file. This means for every csd in the second
# file, there is an equivalent or minimal csd in the first file. This assumes the files are of the format output by the
# main function in csdtaneFinal.py or csdtaneFinalParallel.py
def check_csd_file_covers_csd_file(csd_file1, csd_file2):
    f = open(csd_file1,"r")
    csd1_lines = f.readlines()
    f.close()
    f = open(csd_file2,"r")
    csd2_lines = f.readlines()
    f.close()
    csd1_lines = map(lambda x: x.replace("\n",""), csd1_lines)
    csd2_lines = map(lambda x: x.replace("\n",""), csd2_lines)
    csds1 = []
    csds2 = []
    for csd in csd1_lines:
        csds1.append(create_csd_from_csd_string(csd))
    for csd in csd2_lines:
        csds2.append(create_csd_from_csd_string(csd))
    return check_left_csds_cover_right_csds(csds1,csds2)

# checks whether the csds in the first file cover the cfds in the second file. This means for every cfd in the second
# file, there is an equivalent or minimal csd in the first file. This assumes the files are of the format output by the
# main functions in csdtaneFinal.py or csdtaneFinalParallel.py and ctaneFinal.py or ctaneFinalParallel.py
def check_csd_file_covers_cfd_file(csd_file, cfd_file):
    f = open(csd_file,"r")
    csd_lines = f.readlines()
    f.close()
    f = open(cfd_file,"r")
    cfd_lines = f.readlines()
    f.close()
    csd_lines = map(lambda x: x.replace("\n",""), csd_lines)
    cfd_lines = map(lambda x: x.replace("\n",""), cfd_lines)
    csds = []
    cfds = []
    for csd in csd_lines:
        csds.append(create_csd_from_csd_string(csd))
    for cfd in cfd_lines:
        cfds.append(create_csd_from_cfd_string(cfd))
    return check_left_csds_cover_right_csds(csds,cfds)

# checks whether the first list of csds cover the second list of csds. Thes lists are assumed to be lists of CSD
# objects.
def check_left_csds_cover_right_csds(left_csds, right_csds):
    for csd1 in right_csds:
        found = False
        for csd2 in left_csds:
            if csd2.minimal_compared_to(csd1):
                found = True
                break
        if not found:
            print("error with the following: ")
            print(str(csd1))
            return False
    return True

# finds CFD's with an empty LHS, this means attributes with only one value over the whole table. It then adds 2 CSD's
# to the lists of all CSDs (the CSDs of a CFD with a wildcard and with a constant) and returns all the 'static'
# attributes as a list
def find_empty_lhs_cfds_as_csds(data, allCSDs, allCSDStrings, find_ping = False):
    result = []
    for col in data.columns.values:
        first_val = data[col].values[0]
        all_same = True
        for val in data[col].values:
            if val != first_val:
                all_same = False
                break
        if all_same:
            result.append(col)
            csd1 = CSD([],[col],[],[WILDCARD_SYMBOL])
            csd2 = CSD([],[col],[],[{str(first_val)}])
            if find_ping:
                print("CSD found: ", str(csd1))
                print("CSD found: ", str(csd2))
            allCSDs.append(csd1)
            allCSDStrings.append(str(csd1))
            allCSDs.append(csd2)
            allCSDStrings.append(str(csd2))
    return result

# Generates the first level of the lattice and its partitions, given a certain data set, clustering and support
# threshold (A manager is also given to manage the shared data result). The level is a dict and returned as the first
# value, the partitions are also given by a dict and are the second value of the returned tuple.
def generate_level_1_and_partitions(data, clustered_dict, support):
    # attribute_partitions are dicts with key: ((X,),("--",)) (X being only one attribute) and value: the equivalence
    # classes of the partitions with that attribute and the wildcard set-pattern tuple.
    result_level_1 = {}
    result_partitions = dict()
    attribute_partitions = generate_attribute_partitions(data)
    single_attribute_partitions = dict()
    # in this special case, 'attributes' and 'pattern' are one-element tuples
    for (attributes, pattern) in attribute_partitions:
        partition = attribute_partitions[(attributes, pattern)]
        result_level_1[attributes] = [pattern]
        result_partitions[(attributes, pattern)] = remove_ints_from_partition(partition)
        # this var is to help with figuring out which non-singleton set-patterns reach the support threshold and how to
        # add those to the final result
        vals_and_eq_classes = {}
        for eq_class in partition:
            # Check if the equivalence class is a frozenset (more than one elem). If not, it is just the one index of
            # the equivalence class (an int)
            if isinstance(eq_class, frozenset):
                index = 0
                for i in eq_class:
                    index = i
                    break
                # val is the actual value of the relevant attribute that the equivalence class is matching
                val = str(data.iloc[index][attributes[0]])
                vals_and_eq_classes[val] = eq_class
                if len(eq_class) >= support:
                    result_level_1[attributes].append((frozenset([val]),))
                    result_set = set([eq_class])
                    result_partitions[(attributes, (frozenset([val]),))] = result_set
            # If the equivalence class consists of one element, and the support is one, it must also be considered
            elif isinstance(eq_class, int):
                val = str(data.iloc[eq_class][attributes[0]])
                vals_and_eq_classes[val] = frozenset([eq_class])
                if support == 1:
                    result_level_1[attributes].append((frozenset([val]),))
                    result_set = set([frozenset([eq_class])])
                    result_partitions[(attributes, (frozenset([val]),))] = result_set
        # this temporary dictionary is to help create the relevant single attribute partitions
        temp_dict = dict()
        for set_pattern in clustered_dict[attributes[0]]:
            eq_class_set = frozenset([])
            for inner_pattern in set_pattern:
                eq_class_set = eq_class_set | vals_and_eq_classes[inner_pattern]
            # eq_class_set is the equivalence class for the set of constants
            if len(eq_class_set) >= support:
                for inner_pattern in set_pattern:
                    if inner_pattern not in temp_dict:
                        # this updates the temporary dictionary as needed (temp_dict keeps the singleton sets and their
                        # partitioning of the data
                        temp_dict[inner_pattern] = vals_and_eq_classes[inner_pattern]
                inserted = False
                # this inserts the new set-pattern into the correct place in the level
                for level_1_pattern in result_level_1[attributes]:
                    if sp_tuples_match(level_1_pattern, (set_pattern,)):
                        result_level_1[attributes].insert(result_level_1[attributes].index(level_1_pattern),
                                                          (frozenset(set_pattern),))
                        inserted = True
                        break
                if not inserted:
                    result_level_1[attributes].append((frozenset(set_pattern),))
                result_set = set([eq_class_set])
                result_partitions[(attributes, (frozenset(set_pattern),))] = result_set
        if len(temp_dict) != 0:
            single_attribute_partitions[attributes[0]] = temp_dict
    return result_level_1, result_partitions, single_attribute_partitions

# a simple method to remove ints from a partition (where the ints represent singleton eq_classes)
def remove_ints_from_partition(partition):
    result = {i for i in partition if not isinstance(i,int)}
    return result

# Generates the attribute partitions of a given dataset. These are the same as the (old) partitioning done according to
# one attribute and a wildcard pattern tuple. The old partitioning (and thus, these attribute partitions) keep both
# frozensets (equivalence classes with multiple values) and ints (singleton equivalence classes). The result is a dict.
def generate_attribute_partitions(data):
    result = {}
    # temp_result will temporarily store the partitions as follows: The outer layer is a dict with the attributes as
    # keys. Each value is a defaultdict, with the tuple value (from the table) determining an equivalence class as key
    # and the equivalence classes (set of indices) as values.
    temp_result = {}
    for col in data.columns.values:
        temp_result[col] = defaultdict(set)
    for index in range(len(data)):
        data_tuple = data.iloc[index]
        for col in temp_result:
            temp_result[col][data_tuple[col]].add(index)
    for col in temp_result:
        eq_classes = set()
        for eq_class in temp_result[col]:
            if len(temp_result[col][eq_class]) > 1:
                eq_classes.add(frozenset(temp_result[col][eq_class]))
            elif len(temp_result[col][eq_class]) == 1:
                eq_classes.add(temp_result[col][eq_class].pop())
        result[((col,),(WILDCARD_SYMBOL,))] = eq_classes
    return result

# Generates the cplus sets for the first level of the lattice, given that first level. Elements in the level with a
# wildcard in the set-pattern tuple only contain tuples with wildcards in their cplus set. An element (X,t) with X a
# singleton attribute and t a singleton set-pattern tuple does not contain any (X,t') in its cplus with t' != t. Other
# than these rules, a cplus set is equal to the first level.
def generate_first_cplus(level_1, input_cols_list = list([])):
    result = {}
    level_1_only_wildcards = {}
    for attr in level_1:
        level_1_only_wildcards[attr] = [(WILDCARD_SYMBOL,)]
    for attr in level_1:
        for pattern in level_1[attr]:
            if pattern[0] == WILDCARD_SYMBOL:
                result[(attr, pattern)] = dict(level_1_only_wildcards)
            else:
                result[(attr, pattern)] = filter_first_cplus_set(level_1,attr,pattern)
            if attr[0] in input_cols_list:
                del result[(attr, pattern)][attr]
    return result

# filters a candidate cplus set. It is assumed the set given (=level) is almost correct, but the value of key "attr" has
# 'too many values'. Basically, it must contain the given pattern, and if it does, a singleton list of this pattern will
#  be the value to the "attr" key in the returned dict
def filter_first_cplus_set(level, attr, pattern):
    result = dict(level)
    if pattern in result.pop(attr, []):
        result[attr] = [pattern]
    else:
        result.pop(attr, None)
    return result

# Generates the next level and partitions, given  the previous partitions and support. The result is a tuple of dicts
def generate_next_level_and_partitions(level, partitions, support, single_attribute_partitions):
    result_partitions = generate_next_partitions(level,partitions,support,single_attribute_partitions)
    result_level = generate_level_from_partitions(result_partitions)
    return result_level, result_partitions

# Generates the next partitions given the previous partitions, support and single attribute partitions. (closely related
#  to Step 4 of CTANE as explained in the paper "Discovering Conditional Functional Dependencies").
def generate_next_partitions(level,previous_partitions, support, single_attribute_partitions):
    partitions_keys = list(previous_partitions.keys())
    checked_set = set()
    result = dict()
    level_keys = list(level.keys())
    # counter = 200
    for outer_index in range(len(level_keys)):
        for inner_index in range(outer_index + 1, len(level_keys)):
            attrs_agree, outer_attr, inner_attr = agree_on_all_attributes_but_one(level_keys[outer_index],
                                                                                  level_keys[inner_index])
            if not attrs_agree:
                continue
            for outer_set_pattern_tup in level[level_keys[outer_index]]:
                for inner_set_pattern_tup in level[level_keys[inner_index]]:
                    outer_elem = (level_keys[outer_index],outer_set_pattern_tup)
                    inner_elem = (level_keys[inner_index],inner_set_pattern_tup)
                    agree, new_elem = agree_on_all_but_one_and_combine_given_attr_diff(outer_elem, outer_attr,
                                                                                       inner_elem, inner_attr)
                    if agree:
                        # It could be that the new element was already checked, in that case it doesn't need to be done
                        # again
                        if already_checked(new_elem, checked_set):
                            continue
                        elif check_all_sub_elems_present(new_elem, partitions_keys):
                            new_partition = partition_intersection(outer_elem, previous_partitions[outer_elem],
                                                                   inner_elem, previous_partitions[inner_elem])
                            # This first check is to find out if all constants appearing in the sets of the set-pattern
                            # tuple also appear somewhere in the newly created partition. This is the only place
                            # single_attribute_partitions is actually used.
                            if not partition_is_set_k_frequent(new_elem, new_partition, single_attribute_partitions):
                                continue
                            if no_wildcards(new_elem[1]):
                                if not reaches_support_threshold(support, new_partition):
                                    continue
                            result[new_elem] = new_partition
                            checked_set.add(new_elem)
    return result

# checks if two sets of attributes only differ in 1 attribute. If not, return False and 2 empty strings. If yes, return
# True and the differing left and right attribute
def agree_on_all_attributes_but_one(left_attrs, right_attrs):
    extra_left_attrs = set(left_attrs) - set(right_attrs)
    extra_right_attrs = set(right_attrs) - set(left_attrs)
    if len(extra_left_attrs) != 1:
        return False, "", ""
    else:
        return True, extra_left_attrs.pop(), extra_right_attrs.pop()

def agree_on_all_but_one_and_combine_given_attr_diff(left_elem,left_attr,right_elem,right_attr):
    right_pattern = right_elem[1][right_elem[0].index(right_attr)]
    left_attr_l = list(left_elem[0])
    left_pattern_l = list(left_elem[1])
    right_pattern_l = list(right_elem[1])
    del left_pattern_l[left_elem[0].index(left_attr)]
    del right_pattern_l[right_elem[0].index(right_attr)]
    if left_pattern_l != right_pattern_l:
        return False, ()
    # The temp_result is the complete left element. This is assumed to be sorted by the attribute values.
    temp_result = [left_attr_l, list(left_elem[1])]
    # because temp_result is assumed to be sorted, the new attribute can be "inserted into a sorted list" (=insort)
    insort(temp_result[0], right_attr)
    # the related value for the set-pattern tuple must be inserted in its correct place (same place as its attribute)
    temp_result[1].insert(temp_result[0].index(right_attr), right_pattern)
    return True, (tuple(temp_result[0]), tuple(temp_result[1]))


# Both elements must be tuples (X,t) where X is a tuple of attributes and t is a set-pattern tuple. This method checks
# if the two elements agree on every attribute and related set-pattern tuple value, except for one. The inputs are
# assumed to have the same size as each other. Elements that agree on all attributes are not allowed. Both elements are
# assumed to be sorted alphabetically by the attribute values.
# If the elements agree on all but one (the first result is True), then the second result will be the combination of
# both elements, with the combination being correctly ordered. Otherwise it is an empty tuple.
def agree_on_all_but_one_and_combine(left_elem, right_elem):
    extra_left_attrs = set(left_elem[0]) - set(right_elem[0])
    extra_right_attrs = set(right_elem[0]) - set(left_elem[0])
    if len(extra_left_attrs) != 1:
        return False, ()
    left_attr = extra_left_attrs.pop()
    right_attr = extra_right_attrs.pop()
    right_pattern = right_elem[1][right_elem[0].index(right_attr)]
    left_attr_l = list(left_elem[0])
    left_pattern_l = list(left_elem[1])
    right_pattern_l = list(right_elem[1])
    del left_pattern_l[left_elem[0].index(left_attr)]
    del right_pattern_l[right_elem[0].index(right_attr)]
    if left_pattern_l != right_pattern_l:
        return False, ()
    # The temp_result is the complete left element. This is assumed to be sorted by the attribute values.
    temp_result = [left_attr_l, list(left_elem[1])]
    # because temp_result is assumed to be sorted, the new attribute can be "inserted into a sorted list" (=insort)
    insort(temp_result[0], right_attr)
    # the related value for the set-pattern tuple must be inserted in its correct place (same place as its attribute)
    temp_result[1].insert(temp_result[0].index(right_attr), right_pattern)
    return True, (tuple(temp_result[0]), tuple(temp_result[1]))

# Check that each constant in each set of the set-pattern tuple in the given element appears somewhere in the given
# partition.
def partition_is_set_k_frequent(elem,partition,single_attribute_partitions):
    all_matching_indices = {item for froset in partition for item in froset}
    for pattern_index in range(len(elem[1])):
        set_pattern = elem[1][pattern_index]
        # if the set-pattern is the wildcard symbol, it doesn't need to be checked
        if set_pattern==WILDCARD_SYMBOL:
            continue
        # if the set-pattern is a singleton, it doesn't need to be checked: all tuples in the eq_class of the partition
        # match this part of the set-pattern tuple by construction
        elif len(set_pattern) == 1:
            continue
        # finally, if the set-pattern is a set of multiple constants, the single attribute partition of each constant
        # is checked to have overlapping values with the given partition
        else:
            attr = elem[0][pattern_index]
            for pattern in set_pattern:
                single_attr_eq_class = single_attribute_partitions[attr][pattern]
                union_set = {i for i in single_attr_eq_class if i in all_matching_indices}
                if len(union_set) == 0:
                    return False
    return True

# Checks if elem is in checked_set.
def already_checked(elem, checked_set):
    return elem in checked_set

# Checks whether two elements (a tuple with attributes on the left and a pattern tuple on the right) are the same. The
# attributes do not need to be in the same order, but both elements need to have the same attributes, and the pattern
# tuple needs to have matching elements, following the order of the attributes.
# eg: same elements: (("AC","CC"),("908","--")) and (("CC","AC"),("--","908"))
#     not same elements: (("AC","CC"),("908","--")) and (("CC","AC"),("--","--"))
#     not same elements: (("AC","CC"),("908","--")) and (("CC","AC"),("908","--"))
#     not same elements: (("AC","CC"),("--","--")) and (("CC","ZIP"),("--","--"))
# NOTE: this is not used anymore, because elems are always sorted by attribute...
def same_elems(left_elem, right_elem):
    if len(left_elem[0]) != len(right_elem[0]):
        return False
    left_elem_attr_s = set(left_elem[0])
    right_elem_attr_s = set(right_elem[0])
    if left_elem_attr_s != right_elem_attr_s:
        return False
    for attr in left_elem_attr_s:
        i1 = left_elem[0].index(attr)
        i2 = right_elem[0].index(attr)
        val1 = left_elem[1][i1]
        val2 = right_elem[1][i2]
        if val1 != val2:
            return False
    return True

# Computes the partition intersection of the given partitions. It is assumed both elems "agree on all but one".
def partition_intersection(left_element, left_partition, right_element, right_partition):
    result = set()
    # This is a heuristic to try to use the partition with less equivalence classes that match its set-pattern tuple.
    # This also makes sure that if the current element's set-pattern tuple consists only of wildcards, then the other
    # element's set-pattern tuple also only consists of wildcards.
    if nb_of_wildcards(left_element[1]) < nb_of_wildcards(right_element[1]):
        curr_elem = left_element
        curr_part = left_partition
        other_elem = right_element
        other_part = right_partition
    else:
        curr_elem = right_element
        curr_part = right_partition
        other_elem = left_element
        other_part = left_partition
    for eq_class in curr_part:
        # If the other element's set-pattern tuple consists of only wildcards, then all tuples match that set-pattern
        # tuple. Therefore, ints that are in a frozenset from the current partition (match the current elem's tuple) but
        # do not appear in the other partition will still match the new elem's tuple, but will also be eq_classes of
        # size one (singleton classes).
        if only_wildcards(other_elem[1]) & (not only_wildcards(curr_elem[1])):
            all_frozensets_combined = {item for froset in other_part for item in froset}
            now_singleton_classes = {frozenset([item]) for item in eq_class if item not in all_frozensets_combined}
            result = result | now_singleton_classes
        # set intersection is used to determine the other equivalence classes.
        split_classes =  {items & eq_class for items in other_part if isinstance(items, frozenset)
                          and len(items & eq_class) != 0}
        result = result | split_classes
    return result

# Returns the amount of wildcards in the given pattern tuple
def nb_of_wildcards(tup):
    return tup.count(WILDCARD_SYMBOL)

# Checks if there are any wildcards present in the given pattern tuple. True means there are 0 wildcards present.
def no_wildcards(tup):
    return nb_of_wildcards(tup) == 0

# Checks if the given pattern tuple consists only of wildcards
def only_wildcards(tup):
    return nb_of_wildcards(tup) == len(tup)

# Determines whether the given partition of some element has a support equal to or greater than the support given. It is
# assumed that the element's set-pattern tuple (the element itself is not needed, thus it is not in the input) consists
# of sets of constants only. It does this by checking the only matching equivalence class. If the partition is from an
# element with only sets of constants in its set-pattern tuple, the partitioning should be only one frozenset,
# this is the equivalence class that matches the pattern tuple, and its length must be at least equal to the support.
def reaches_support_threshold(support, elem_partition):
    matching_eq_classes = elem_partition.copy()
    if len(matching_eq_classes) == 0:
        return False
    elif len(matching_eq_classes.pop()) >= support:
        return True
    else:
        return False

# Checks if all 'sub-elements' of a given element are present in the previous partitions. This is the check for
# Step 4 (b) (iii) in the paper.
def check_all_sub_elems_present(new_elem, previous_partitions_keys):
    for attr in new_elem[0]:
        temp_elem = [list(new_elem[0]), list(new_elem[1])]
        del temp_elem[1][new_elem[0].index(attr)]
        temp_elem[0].remove(attr)
        check_elem = (tuple(temp_elem[0]), tuple(temp_elem[1]))
        if not (check_elem in previous_partitions_keys):
            return False
    return True

# Generates a level that corresponds to the given partition. This assumes the tuple of attributes given as the first
# part of the key of partitions is sorted alphabetically.
def generate_level_from_partitions(partitions):
    result = defaultdict(list)
    for (attr,tup) in partitions.keys():
        result[attr].append(tup)
    for attr in result:
        # This sorts the list of set-pattern tuples (by generality/ matching-ness) for each attribute tuple, which the
        # algorithm must keep valid.
        # The sorting is done by sorting one position in the tuple: wildcards -> biggest set -> smallest set. This
        # sort is then repeated for every position in the tuple.
        tup_len = len(result[attr][0])
        # this could just be a normal range(tup_len), but using this range will make the first tuples in the sorted list
        # have a wildcard in tp[0]
        for i in range(tup_len-1, -1, -1):
            result[attr].sort(key = key_fnc(i), reverse = True)
    return result

#creates a key function for use in sorting levels
def key_fnc(index):
    return lambda x: 1 if x[index]==WILDCARD_SYMBOL else -1/len(x[index])

# generates the cplus (dict of dicts) given the current level and previous cplus
def generate_cplus(current_level, previous_cplus):
    result = {}
    for attrs in current_level:
        for tup in current_level[attrs]:
            temp_result_list = []
            for attr in attrs:
                temp_elem = [list(attrs), list(tup)]
                del temp_elem[1][temp_elem[0].index(attr)]
                temp_elem[0].remove(attr)
                elem = (tuple(temp_elem[0]), tuple(temp_elem[1]))
                temp_result_list.append(previous_cplus[elem])
            result[(attrs,tup)] = cplus_intersect_list(temp_result_list)
    return result

# calculates the intersection of a list of "cplus values". These cplus values are similar to the first level, with
# possibly some elements pruned
def cplus_intersect_list(cplus_list):
    return reduce(lambda acc, next_val: cplus_intersect(acc, next_val), cplus_list)

# calculates the intersection of two "cplus values".
def cplus_intersect(cplus1, cplus2):
    result = {}
    both_keys = {key for key in cplus1 if key in cplus2}
    for key in both_keys:
        temp_list = [val for val in cplus1[key] if val in cplus2[key]]
        if len(temp_list) != 0:
            result[key] = temp_list
    return result

# This method returns the new (pruned) CPlus, based on the given CPlus. It also updates the variable allCSDs.
# The -Step suffix refers to the fact that it is step 2 of the algorithm. The algorithm needs the current and previous
# partitions as well as the amount of tuples (row_count) to efficiently calculate if a CSD holds.
# Because of the expanded definition of minimal CSDs, when a constant CSD is found, a forward check must be done to see
# if there is a minimal version of the found CSD. It is guaranteed to only add the minimal CSD
def find_csds_step(current_level, temp_cplus, current_partitions, previous_partitions, row_count,
                   allCSDs, allCSDStrings, find_ping = False):
    final_cplus = temp_cplus
    for attrs in current_level:
        for tup in current_level[attrs]:
            for attr in attrs:
                # this function will return the minimal CSD
                found_csd = look_for_specific_csd(attr, attrs, current_partitions, final_cplus, previous_partitions,
                                                  tup, row_count)
                if found_csd is not None:
                    if find_ping:
                        print("CSD found: ", str(found_csd))
                    allCSDs.append(found_csd)
                    allCSDStrings.append(str(found_csd))
    remove_empty_cpluses(final_cplus)
    return final_cplus

# Finds a specific CSD given some parameters. The CSD will be 'minimal'.
def look_for_specific_csd(attr, attrs, current_partitions, final_cplus, previous_partitions, tup, row_count):
    cplus_X_sp = final_cplus[(attrs, tup)]
    if (attr,) in cplus_X_sp and len(cplus_X_sp[(attr,)]) >= 1:
        # normally, cplus_X_sp[(attr,)] should be a list of one element, and that one element is a tuple of
        # one element (set of constants or wildcard symbol)
        cA = cplus_X_sp[(attr,)][0][0]
        full_elem = (attrs, tup)
        temp_tup = list(tup)
        del temp_tup[attrs.index(attr)]
        temp_attrs = list(attrs)
        temp_attrs.remove(attr)
        reduced_elem = (tuple(temp_attrs), tuple(temp_tup))
        if partition_length_equal(current_partitions, previous_partitions, full_elem, reduced_elem, row_count):
            prune_cplus(final_cplus, attrs, tup, attr, cA)
            found_csd = CSD(temp_attrs, [attr], temp_tup, [cA])
            return found_csd
    return None


# Checks if the partition length from curr_part of full_elem is equal to the partition length from prev_part of
# reduced_elem. Because no ints (singleton eq_classes) are saved in the partitions, the total amount of tuples
# (row_count) must be used to determine how many equivalence classes there are. Note that no ints are saved in the
# partition, but frozensets of one int could be saved in the partition.
def partition_length_equal(curr_part, prev_part, full_elem, reduced_elem, row_count):
    curr_eq_class_item_count = 0
    for eq_class in curr_part[full_elem]:
        curr_eq_class_item_count += len(eq_class) - 1
    prev_eq_class_item_count = 0
    for eq_class in prev_part[reduced_elem]:
        prev_eq_class_item_count += len(eq_class) - 1
    curr_part_size = row_count - curr_eq_class_item_count
    prev_part_size = row_count - prev_eq_class_item_count
    return curr_part_size == prev_part_size

# prunes the given cplus by the rules defined in step 2 of the CTANE algorithm (see paper), extended with rules for CSDs
def prune_cplus(cplus, attrs, tup, attr_A, cA):
    prune_set = set([])
    for elem in cplus:
        if elem[0] != attrs:
            continue
        index_A = elem[0].index(attr_A)
        if not (sp_tuples_match((cA,), (elem[1][index_A],))):
            continue
        temp_elem_pattern = list(elem[1][:index_A] + elem[1][index_A+1:])
        temp_tup_pattern = list(tup)
        del temp_tup_pattern[attrs.index(attr_A)]
        if sp_tuples_match(tuple(temp_elem_pattern), tuple(temp_tup_pattern)):
            prune_set.add(elem)
    for elem in prune_set:
        curr_dict = cplus[elem]
        if (attr_A,) in curr_dict:
            del curr_dict[(attr_A,)]
        keys = list(curr_dict.keys())
        for attr in keys:
            # attr should be a tuple of one element
            if not attr[0] in attrs:
                del curr_dict[attr]
    return

# Determines whether the left set-pattern tuple matches to the right set-pattern tuple: left_tuple =< right_tuple
# note: this has been updated for CSDs
def sp_tuples_match(left_tuple, right_tuple):
    if len(left_tuple) != len(right_tuple):
        return False
    else:
        for index in range(len(right_tuple)):
            if right_tuple[index] == WILDCARD_SYMBOL:
                if (left_tuple[index] == WILDCARD_SYMBOL) or (len(left_tuple[index]) == 1):
                    continue
                else:
                    return False
            else:
                if left_tuple[index] == WILDCARD_SYMBOL:
                    return False
                else:
                    if left_tuple[index].issubset(right_tuple[index]):
                        continue
                    else:
                        return False
        return True

# Removes inner dictionaries from the given cplus if they are empty.
def remove_empty_cpluses(cplus):
    keys = list(cplus.keys())
    for key in keys:
        if len(cplus[key]) == 0:
            del cplus[key]
    return

#Step 3 of CTANE (see paper). Returns the pruned current partitions, using the current cplus.
def prune_partitions_and_L(current_cplus, current_partitions, current_level):
    result_partitions = current_partitions
    result_level = current_level
    for elem in list(current_partitions.keys()):
        if not elem in current_cplus:
            del result_partitions[elem]
            result_level[elem[0]].remove(elem[1])
    for attrs in set(result_level.keys()):
        if len(result_level[attrs]) == 0:
            del result_level[attrs]
    return result_partitions, result_level