import sys
from ctaneFunctions import *


def ctane_main(infile="", k=1, to_output=False, find_ping=False, input_cols_str=""):
    allCFDs = []
    allCFDStrings = []
    input_cols_list = []
    if input_cols_str != "":
        input_cols_list = input_cols_str.split(",")

    level_no = 1

    # read the csv file, assuming all tuple elements to be read as 'objects'
    csv_data = read_csv(infile, dtype=object)
    # replace "NaN" values with the empty string
    csv_data = csv_data.fillna("")

    # INITIALIZATION, this technically includes the iteration of l=1

    # find all CFDs with an empty LHS, add them to the list of all CFDs. The result of the method returns a list of all
    # columns that are in such an CFD (here they are called static columns because they always contain 1 value)
    staticCols = find_empty_lhs_cfds(csv_data, allCFDs, allCFDStrings, find_ping)
    csv_data = csv_data.drop(columns=staticCols)
    if find_ping:
        print("current level: ", level_no)
    # generate L1 from the data. L1 is a dictionary where the keys are a tuple of attributes and the values are a list
    # of pattern tuples, eg: L1 == {("CC","AC"): [("--","--"), ("--","908"), ("01","--")], ...}. Also generate the
    # partitions of L1 with the given data. The partitions are a dictionary, with the key being a tuple of 2 tuples (the
    #  first being the attributes, the second being the pattern tuple) and the values are the indices of the csv file's
    # tuples, partitioned into their equivalence classes. For more info, see comments at the top of ctaneFunctions.py.
    (L1, L1Partitions) = generate_level_1_and_partitions(csv_data, k)
    # generate the (first) C+ set for each element of L1 (C+ represents the RHS candidates)
    CPlus = generate_first_cplus(L1, input_cols_list)
    # generate the next L and partitions of L.
    nextLAndPartitions = generate_next_level_and_partitions(L1Partitions, k)
    # Assign some certain variables with names to make things clear.
    previousPartitions = L1Partitions
    previousCPlus = CPlus
    currentL = nextLAndPartitions[0]
    currentPartitions = nextLAndPartitions[1]
    # While the current level isn't empty, do the following
    while len(currentL) != 0:
        level_no += 1
        if find_ping:
            print("current level: ", level_no)
        # Step 1) generate C+ for the current level, given the previous C+
        tempCPlus = generate_cplus(currentL, previousCPlus)
        # Step 2) for each (X,sp) in currentL, look for valid CFDs. This method returns the new (pruned) CPlus, based on
        # the given CPlus. It also updates 'allCFDs'. The _step suffix refers to the fact that it is step 2 of the
        # algorithm. The algorithm needs the current and previous partitions to efficiently calculate if a CFD holds.
        currentCPlus = find_cfds_step(currentL, tempCPlus, currentPartitions, previousPartitions, allCFDs,
                                      allCFDStrings, find_ping)
        # Step 3) prune away elements of the current level that have an empty C+. In practice, this only actually prunes
        # and returns the partitions.
        previousPartitions = prune_partitions(currentCPlus, currentPartitions)
        # Step 4) generate the next level (and partitions)
        nextLAndPartitions = generate_next_level_and_partitions(previousPartitions, k)
        previousCPlus = currentCPlus
        currentL = nextLAndPartitions[0]
        currentPartitions = nextLAndPartitions[1]

    # print what was found
    if not find_ping:
        print("List of all CFDs: ")
        for CFDString in allCFDStrings:
            print(CFDString)
    print("Total number of CFDs found: ", len(allCFDs))

    if to_output:
        new_name = ".".join(infile.split(".")[:-1])
        if len(new_name) == 0:
            new_name = infile
        new_name = new_name + "_support_" + str(k) + "_CFDs.txt"
        outfile = open(new_name, 'w')
        for cfd_str in allCFDStrings:
            outfile.write("%s\n" % cfd_str)

    return allCFDs

#------------------------------------------------------- START ---------------------------------------------------
if __name__ == '__main__':
    infile = ""
    # if support not given, assume 1
    k = 1
    to_output = False
    find_ping = False
    input_cols_str = ""
    if len(sys.argv) > 1:
        infile=str(sys.argv[1])
    if len(sys.argv) > 2:
        k=int(sys.argv[2])
    if len(sys.argv) > 3:
        to_output=bool(int(sys.argv[3]))
    if len(sys.argv) > 4:
        find_ping=bool(int(sys.argv[4]))
    if len(sys.argv) > 5:
        input_cols_str = str(sys.argv[5])
        if input_cols_str == " ":
            input_cols_str = ""
    ctane_main(infile,k,to_output,find_ping,input_cols_str)
