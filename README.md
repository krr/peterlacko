###### This repository contains code for the CTANE and CSDTANE algorithms, as well as some example datasets. This was made for the thesis paper "Extracting Knwoledge from a Database: What Does the Database Know" by Peter Lacko.

###### The 5 main python scripts (ctaneFinal.py, ctaneFinalParallel.py, csdtaneFinal, csdtaneFinalNoMatching and csdtaneFinalParallel.py) are meant to run with python 3.6. It is also recommended to use the 64 bit version of python 3.6. If the 32 bit version of python 3.6 is used to run the scripts with big data files, this can result in much longer run times or MemoryError crashes.

###### The scripts also require the pandas, sklearn, scipy and hdbscan package. To install packages with pip, see <https://docs.python.org/3/installing/index.html>

### CTANE:

ctaneFunctions.py contains many functions used by the ctane implementation. It is commented, so look at it to get an idea of how the code works.

ctaneFinal.py is the actual python file for discovering CFDs from a csv input file (sequentially). ctaneFinalParallel also implements CTANE, but in a parallel way.

The algorithm is based on the CTANE algorithm from the paper "Discovering Conditional Functional Dependencies" (link: <http://homepages.inf.ed.ac.uk/fgeerts/pdf/CFDdiscovery.pdf>).
The implementation is also loosely based on the implementation made for the paper "Automatic Discovery of Functional Dependencies and Conditional Functional Dependencies: A Comparative Study" (link: <https://cs.uwaterloo.ca/~nasghar/848.pdf>)

#### Running the code

To run ctaneFinal.py on some data (csv file, eg "DiscoveringCFDsExample.csv"), execute the following command in your terminal:
```
python3 ctaneFinal.py DiscoveringCFDsExample.csv
```

further command line arguments give other options. These are (in order): the support threshold (default = 1), a toggle for writing the found CFDs to an output file (default = 0, off), and a toggle for pinging (simple print) when CFDs are found and a new level in the lattice is reached (default = 0, off)

The following is the same as the first example
```
python3 ctaneFinal.py DiscoveringCFDsExample.csv 1 0 0
```

The following searches for CFDs with support = 3 (at least 3 tuples exist that follow each CFD that is found)
```
python3 ctaneFinal.py DiscoveringCFDsExample.csv 3 0 0
```

The following will not write to an output file, and will print to the console (ping) when a CFD is found and a new level in the lattice is reached
```
python3 ctaneFinal.py DiscoveringCFDsExample.csv 3 0 1
```

The following will write to an output file (named inputfile + "support_" + k + "_CFDs.txt", so here DiscoveringCFDsExample_support_3_CFDs.txt) and will not ping when a CFD is found.
```
python3 ctaneFinal.py DiscoveringCFDsExample.csv 3 1 0
```

#### Parallel version

Included is code that parallelizes CTANE. It is very similar to the previous code, ctaneFunctionsParallel.py is the parallel version of ctaneFunctions.py (only one method within the code is actually parallelized). ctaneFinalParallel.py is the main code to run the parallelized version.

The first four input arguments of ctaneFinalParallel.py are the exact same as for ctaneFinal.py. There is one extra input argument: the task size. This is default set to 1, and must be an integer. The larger this value, the more coarse grained tasks will become. This may result in a bit longer execution time (but still approximately the same), and possibly less RAM usage. If you do not know what value to use, the default is recommended.

WARNING! The parallel version does have a speedup over the sequential version, but it might use all your computer's processing cores, resulting in 100% CPU usage. It also uses more RAM, and is all around a much more computationally intensive program than ctaneFinal.py.

The following command will run the parallel version with support=3, write to an output file (here "DiscoveringCFDsExample_support_3_CFDs.txt"), ping when CFDs are found, and have a task size of 4.
```
python3 ctaneFinalParallel.py DiscoveringCFDsExample.csv 3 1 1 4
```

### CSDTANE:

csdtaneFunctions.py contains the code that is adapted from ctaneFunctions.py and csdtaneFunctionsParallel.py contains code that is adapted from ctaneFunctionsParallel.py to be able to handle CSDs. csdtaneFinal.py contains the code adapted from ctaneFinal.py and csdtaneFinalParallel.py contains the code that is adapted from ctaneFinalParallel.py, which first preprocesses the given data, then searches for CSDs given the original data and the preprocessed data.
Finally, csdtaneFunctionsNoMatching.py and csdtaneFinalNoMatching contains mostly the same code as csdtaneFunctions.py and csdtaneFinal.py, but some sections are removed/commented out. This is to implement the special search for k'-frequent CSDs, explained in the paper "Extracting Knowledge from a Database".

#### Running the code

Most of the input arguments are the same as the ctane versions, but there are new ones added. For clarity, all input arguments will be reviewed.

The first argument is the .csv file to analyze. The second argument is the support of k-frequent CSDs to search for. The third and fourth arguments are new: they are the minimum cluster size and minimum amount of smaples respectively. These are parameters passed on to the clustering algorithm, which is HDBSCAN. More info can be found here: http://hdbscan.readthedocs.io/en/latest/ as well as information on the meaning of the parameters: http://hdbscan.readthedocs.io/en/latest/parameter_selection.html 
Note that the alpha mentioned in the above webpage is kept as its default value, but the clustering_selection_method is set to 'leaf'. This is because smaller clusters is prefered for this application.
The default value for minimum cluster size is 3 and the default for minimum samples is 12. These were chosen after a bit of empirical study, but may not be the optimal values. However, due to the complexity of clustering, the default values are still recommended.
The next 2 input parameters are as before: a toggle for writing to file and a toggle for printing output during execution. The defaults are also the same as before.
All these input parameters are the same, no matter which csdtane version is used. The next parameters differ depending on the csdtane version.

When using csdtaneFinal.py or csdtaneFinalNoMatching.py, the next parameter was specially designed for the case study in the paper "Extracting Knowledge from a Database". It denotes which attributes are input attributes and can be safely ignored when creating the initial C plus sets. In general, this is best set to empty (a string containing a space, " "). The final input parameter determines the preprocessing method: 0 uses clustering (using the earlier input parameters). -1 uses complement sets and any number larger than 0 uses partial enumeration. These final two parameters are best left alone, unless you know what you are doing.

When using csdtaneFinalParallel.py, the final input parameter is the task size, same as with ctaneFinalParallel.py. The two extra parameters mentioned above for csdtaneFinal.py and csdtaneFinalNoMatching.py were not implemented for use in the parallel version.

The following command will search for CSDs (sequentially) with the input arguments from left to right as follows: support=3, min_cluster_size=2, min_samples=4, write to an output file (here "StudentsExample_support_3_CSDs.txt"), ping when CSDs are found, no attributes are input attributes and clustering is used.
```
python3 csdtaneFinal.py StudentsExample.csv 3 2 4 1 1
```

More explicitly, this can be written as
```
python3 csdtaneFinal.py StudentsExample.csv 3 2 4 1 1 " " 0
```

The following command will search for CSDs (sequentially) with the input arguments from left to right as follows: support=3, min_cluster_size=2, min_samples=4 (both not used), write to an output file (here "StudentsExample_support_3_CSDs.txt"), ping when CSDs are found, no attributes are input attributes and partial enumeration is used.
```
python3 csdtaneFinal.py StudentsExample.csv 3 2 4 1 1 " " 2
```

The following command will search for CSDs (sequentially, using the special k'-frequency) with the input arguments from left to right as follows: support=3, min_cluster_size=2, min_samples=4 (both not used), write to an output file (here "StudentsExample_support_3_CSDs.txt"), ping when CSDs are found, no attributes are input attributes and complement sets are used.
```
python3 csdtaneFinalNoMatching.py StudentsExample.csv 3 2 4 1 1 " " -1
```

The following command will search for CSDs (in parallel) with the input arguments from left to right as follows: support=2, min_cluster_size=2, min_samples=4, write to an output file (here "StudentsExample_support_2_CSDs.txt"), ping when CSDs are found and have a task size of 1.
```
python3 csdtaneFinalParallel.py StudentsExample.csv 2 2 4 1 1 1
```

Note that the default values of clustering are not recommended when using small database examples (instead use lower values, such as 2 or 3 for both parameters).