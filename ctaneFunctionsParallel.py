from pandas import *
from bisect import *
from collections import defaultdict
from functools import reduce
from multiprocessing import Pool, cpu_count, Manager

WILDCARD_SYMBOL = "--"
allCFDs=[]
allCFDStrings=[]


# Here the data structures used for certain concepts will be explained (note the explanation might repeat in further
# comment sections).

# Element (this is an element of the lattice, or an element of the levels of the lattice):
# An element of the lattice is a set/list/... of attributes and a pattern tuple. This is (usually) represented by a
# tuple of 2 elements: first a tuple of the attributes, then a tuple that is the pattern tuple.

# level (of the lattice):
# this is represented by a dictionary: the keys are (sorted) tuples of attributes. The values are a list of the
# pattern tuples with a set of attributes given in the key. So each element of the level of the lattice can be
# reconstructed by combining each key with each item in its value list. Furthermore, the algorithm must keep every list
# of pattern tuples sorted from most general to least general.

# note: read the paper "Discovering Conditional Functional Dependencies" to see the definition used for creating
# partitions based on elements.

# partition (of the indices of tuples, following a certain element):
# this is represented by a set of frozensets and/or ints. The inner frozensets are always a collection of ints. Each
# element of the set is an equivalence class of the partition. The equivalence class is either a collection of ints;
# these are the indices of the items belonging to the equivalence class. The equivalence class can also be represented
# by an int, in this case it is the index of the single item in the equivalence class. A frozenset of one int is also
# possible (it also represents an equivalence class of one item). The difference between the last two cases depend on
# how the partitioning was made. A partition should always be connected to the element that created the partition; a
# frozenset element represents an equivalence class that matches the pattern tuple of the element that made the
# partitioning. The exception is when the pattern tuple consists only of wildcards. In this case, each equivalence class
# matches the pattern tuple. Since no extra information is gained, single element attributes are represented as ints.
# To summarize: a frozenset of multiple ints is an equivalence class of multiple items that matches the pattern tuple.
# A frozenset of one int is an equivalence class of one item that matches the pattern tuple. Furthermore, the pattern
# tuple should not contain only wildcards. An int is either an equivalence class of one item that does not match the
# pattern tuple, or it is an equivalence class of one item that matches the pattern tuple consisting only of wildcards.

# partitions (all partitions of a level of the lattice):
# this is represented as a dictionary with elements as keys and the corresponding partition (see above) as values.

# Cplus set (of an element of the lattice):
# this is represented as dictionaries nested in a dictionary. The outer dictionary has elements of the current level as
# keys. It's values is a dictionary as follows: the inner dictionary has the same structure as the dictionary for
# levels. It represents all the elements belonging to the Cplus(outer_elem), if outer_elem is the key of the outer
# dictionary. Note that the inner dictionary's keys will always be attribute tuples of length one, due to the definition
# of cplus (the values will also be lists of tuples, where the tuples are length one)

# A class to represent FD's, with an overridden method to convert to string
# all 4 input parameters are assumed to be lists of strings (can be singletons or empty lists)
class CFD:

    def __init__(self, lhs_attributes, rhs_attributes, lhs_pattern, rhs_pattern):
        self.lhs_attributes = lhs_attributes
        self.rhs_attributes = rhs_attributes
        self.lhs_pattern = lhs_pattern
        self.rhs_pattern = rhs_pattern

    def __str__(self):
        return str(self.lhs_attributes) + " -> " + str(self.rhs_attributes) + ", (" + str(self.lhs_pattern)[1:-1] + \
            " || " + str(self.rhs_pattern)[1:-1] + ")"

    def to_FOL(self):
        pass #this would require more info (eg relation names)


# finds CFD's with an empty LHS, this means attributes with only one value over the whole table. It then adds 2 CFD's
# to the lists of all CFDs (a CFD with a wildcard and with a constant) and returns all the 'static' attributes as a list
def find_empty_lhs_cfds(data,find_ping = False):
    global allCFDs
    global allCFDStrings
    result = []
    for col in data.columns.values:
        first_val = data[col].values[0]
        all_same = True
        for val in data[col].values:
            if val != first_val:
                all_same = False
                break
        if all_same:
            result.append(col)
            cfd1 = CFD([],[col],[],[WILDCARD_SYMBOL])
            cfd2 = CFD([],[col],[],[str(first_val)])
            if find_ping:
                print("CFD found: ", str(cfd1))
                print("CFD found: ", str(cfd2))
            allCFDs.append(cfd1)
            allCFDStrings.append(str(cfd1))
            allCFDs.append(cfd2)
            allCFDStrings.append(str(cfd2))
    return result

# Generates the first level of the lattice and its partitions, given a certain data set and support threshold. The
# level is a dict and the first value of the returned tuple, the partitions are also given by a dict and are the second
# value of the returned tuple.
def generate_level_1_and_partitions(data, support, manager):
    # attribute_partitions are dicts with key: ((X,),("--",)) (X being only one attribute) and value: the equivalence
    # classes of the partitions with that attribute and the wildcard pattern tuple.
    result_level_1 = {}
    result_partitions = manager.dict()
    attribute_partitions = generate_attribute_partitions(data)
    # in this special case, 'attributes' and 'pattern' are one-element tuples
    for (attributes, pattern) in attribute_partitions:
        partition = attribute_partitions[(attributes, pattern)]
        result_level_1[attributes] = [pattern]
        result_partitions[(attributes, pattern)] = partition
        for eq_class in partition:
            # Check if the equivalence class is a frozenset (more than one elem). If not, it is just the one index of
            # the equivalence class (an int)
            if isinstance(eq_class, frozenset):
                if len(eq_class) >= support:
                    index = 0
                    for i in eq_class:
                        index = i
                        break
                    # val is the actual value of the relevant attribute that the equivalence class is matching
                    val = str(data.iloc[index][attributes[0]])
                    result_level_1[attributes].append((val,))
                    # the following code fragment 'flattens' the equivalence class. More precisely, it keeps the
                    # frozenset of the relevant eq_class, and all other eq_classes are split up into their individual
                    # items (represented as ints, NOT a frozenset of one int). Frozensets are used to denote that an
                    # eq_class matches the related pattern tuple of the partition, even if that eq_class consists of one
                    # element. An exception is when the pattern tuple contains only wildcards, in this case every
                    # eq_class matches the pattern tuple, so single-value eq_classes are represented as single elements
                    # (ints), not frozensets.
                    temp_part = partition - set([eq_class])
                    result_set = set([eq_class])
                    for temp_eq_class in temp_part:
                        if isinstance(temp_eq_class, frozenset):
                            for item in temp_eq_class:
                                result_set.add(item)
                        elif isinstance(temp_eq_class, int):
                            result_set.add(temp_eq_class)
                    result_partitions[(attributes, (val,))] = result_set
            # If the equivalence class consists of one element, and the support is one, it must also be considered
            elif support == 1 & isinstance(eq_class, int):
                val = str(data.iloc[eq_class][attributes[0]])
                result_level_1[attributes].append((val,))
                # if the eq_class consists of one element, the resulting partitions will be equivalence classes only of
                # length 1, meaning one equivalence class for every tuple (thus the range is used). Care must be taken
                # that the equivalence class of the given attribute and pattern is marked by using a frozenset.
                result_set = set(range(len(data)))
                result_set.discard(eq_class)
                result_set.add(frozenset([eq_class]))
                result_partitions[(attributes, (val,))] = result_set
    return result_level_1, result_partitions

# Generates the attribute partitions of a given dataset. These are the same as the partitioning done according to one
# attribute and a wildcard pattern tuple. The result is a dict.
def generate_attribute_partitions(data):
    result = {}
    # temp_result will temporarily store the partitions as follows: The outer layer is a dict with the attributes as
    # keys. Each value is a defaultdict, with the tuple value (from the table) determining an equivalence class as key
    # and the equivalence classes (set of indices) as values.
    temp_result = {}
    for col in data.columns.values:
        temp_result[col] = defaultdict(set)
    for index in range(len(data)):
        data_tuple = data.iloc[index]
        for col in temp_result:
            temp_result[col][data_tuple[col]].add(index)
    for col in temp_result:
        eq_classes = set()
        for eq_class in temp_result[col]:
            if len(temp_result[col][eq_class]) > 1:
                eq_classes.add(frozenset(temp_result[col][eq_class]))
            elif len(temp_result[col][eq_class]) == 1:
                eq_classes.add(temp_result[col][eq_class].pop())
        result[((col,),(WILDCARD_SYMBOL,))] = eq_classes
    return result

# Generates the cplus sets for the first level of the lattice, given that first level. Elements in the level with a
# wildcard in the pattern tuple only contain tuples with wildcards in their cplus set. an element (X,t) with X a
# singleton attribute and t a singleton pattern tuple does not contain any (X,t') in its cplus, with t' != t. Other than
# these rules, a cplus set is equal to the first level.
def generate_first_cplus(level_1):
    result = {}
    level_1_only_wildcards = {}
    for attr in level_1:
        level_1_only_wildcards[attr] = [(WILDCARD_SYMBOL,)]
    for attr in level_1:
        for pattern in level_1[attr]:
            if pattern[0] == WILDCARD_SYMBOL:
                result[(attr, pattern)] = filter_first_cplus_set(dict(level_1_only_wildcards),attr,pattern)
            else:
                result[(attr, pattern)] = filter_first_cplus_set(dict(level_1),attr,pattern)
    return result

# filters a candidate cplus set. It is assumed the set given (=level) is almost correct, but the value of key "attr" has
# 'too many values'. Basically, it must contain the given pattern, and if it does, a singleton list of this pattern will
#  be the value to the "attr" key in the returned dict
def filter_first_cplus_set(level, attr, pattern):
    result = level
    if pattern in result.pop(attr, []):
        result[attr] = [pattern]
    else:
        result.pop(attr, None)
    return result

# Generates the next level and partitions, given  the previous partitions and support. The result is a tuple of dicts
def generate_next_level_and_partitions(partitions, support, task_size, manager):
    result_partitions = generate_next_partitions(partitions,support, task_size, manager)
    result_level = generate_level_from_partitions(result_partitions)
    return result_level, result_partitions

# Generates the next partitions given the current partitions and support. (closely related to Step 4 of CTANE as
# explained in the paper "Discovering Conditional Functional Dependencies")
# this is the parallel version that parallelizes the task of calculating partition intersections
def generate_next_partitions(previous_partitions, support, task_size, manager):
    processors = cpu_count()
    pool = Pool(processors)
    parallel_map_list = []
    current_task_list = []
    shared_partitions_keys = manager.list(previous_partitions.keys())
    shared_support = manager.Value('i', support)
    input_tup_list = (shared_partitions_keys, previous_partitions, shared_support)
    checked_set = set()
    result = manager.dict()
    # An outer and inner loop is used to check all possible combinations of elements
    for outer_index in range(len(shared_partitions_keys)):
        for inner_index in range(outer_index + 1, len(shared_partitions_keys)):
            (agree, new_elem) = agree_on_all_but_one_and_combine(shared_partitions_keys[outer_index],
                                                                 shared_partitions_keys[inner_index])
            if agree:
                # It could be that the new element was already checked, in that case it doesn't need to be done again
                if already_checked(new_elem, checked_set):
                    continue
                else:
                    current_task_list.append((new_elem, outer_index, inner_index))
                    if len(current_task_list) == task_size:
                        task_inputs = input_tup_list + (current_task_list ,)
                        parallel_map_list.append(task_inputs)
                        current_task_list = []
                    checked_set.add(new_elem)
    task_inputs = input_tup_list + (current_task_list,)
    parallel_map_list.append(task_inputs)
    parallel_result = pool.map(handle_new_elem_parallel, parallel_map_list)
    pool.close()
    for item_list in parallel_result:
        for item in item_list:
            if item != ():
                result[item[0]] = item[1]
    return result

# This is a task for the pool, it takes the input and calculates relevant partition intersections, then checks if those
# intersections are valid and returns an empty tuple if they aren't, if they are valid it returns the new element and
# new partition in a tuple.
def handle_new_elem_parallel(input_tup_list):
    result = []
    partitions_keys = input_tup_list[0]
    previous_partitions = input_tup_list[1]
    support = input_tup_list[2].value
    for input_tup in input_tup_list[3]:
        new_elem = input_tup[0]
        outer_index = input_tup[1]
        inner_index = input_tup[2]
        new_partition = partition_intersection(partitions_keys[outer_index],
                                               previous_partitions[partitions_keys[outer_index]],
                                               partitions_keys[inner_index],
                                               previous_partitions[partitions_keys[inner_index]])
        # If the new element contains wildcards, it automatically reaches the support threshold, because all
        # of its 'sub elements' did in the last level, and adding or removing a wildcard does not change the
        # support of an element. However, if there are no wildcards, we must check the support.
        if no_wildcards(new_elem[1]):
            if not reaches_support_threshold(support, new_partition):
                result.append(())
                continue
        if check_all_sub_elems_present(new_elem, previous_partitions):
            result.append((new_elem, new_partition))
            continue
        else:
            result.append(())
            continue
    return result

# Both elements must be tuples (X,t) where X is a tuple of attributes and t is a pattern tuple. This method checks if
# the two elements agree on every attribute and related pattern tuple symbol, except for one. The inputs are assumed to
# have the same size as each other. Elements that agree on all attributes are not allowed. Both elements are assumed to
# be sorted by the attribute values.
# If the elements agree on all but one (the first result is True), then the second result will be the combination of
# both elements, with the combination being correctly ordered. Otherwise it is an empty tuple.
def agree_on_all_but_one_and_combine(left_elem, right_elem):
    same_attributes = False
    left_elem_attr_s = set(left_elem[0])
    right_elem_attr_s = set(right_elem[0])
    if left_elem_attr_s == right_elem_attr_s:
        # The elems do not agree on all but one (they agree on all)
        return False, ()
    inner_attr = ""
    # Both attribute sets leave out a value, and check if they match
    for outer_index in range(len(left_elem[0])):
        for inner_index in range(len(right_elem[0])):
            outer_attr = left_elem[0][outer_index]
            inner_attr = right_elem[0][inner_index]
            left_elem_attr_s.remove(outer_attr)
            right_elem_attr_s.remove(inner_attr)
            if left_elem_attr_s == right_elem_attr_s:
                same_attributes = True
                break
            else:
                left_elem_attr_s.add(outer_attr)
                right_elem_attr_s.add(inner_attr)
        if same_attributes:
            break
    if not same_attributes:
        # The elems do not agree on all but one (they agree on less, that is more than 1 attribute must be dropped from
        # both attribute sets before the attribute sets agree).
        return False, ()
    else:
        for attr in left_elem_attr_s:
            i1 = left_elem[0].index(attr)
            i2 = right_elem[0].index(attr)
            val1 = left_elem[1][i1]
            val2 = right_elem[1][i2]
            if val1 != val2:
                # The elems do not agree on all but one (the attributes agree on all but one, but the related pattern
                # tuples do not agree.
                return False, ()
    # The temp_result is the complete left element. This is assumed to be sorted by the attribute values.
    temp_result = [list(left_elem[0]), list(left_elem[1])]
    # because temp_result is assumed to be sorted, the new attribute can be "inserted into a sorted list" (=insort)
    insort(temp_result[0],inner_attr)
    # the related value for the pattern tuple must be inserted in its correct place (same place as its attribute)
    temp_result[1].insert(temp_result[0].index(inner_attr), right_elem[1][right_elem[0].index(inner_attr)])
    return True, (tuple(temp_result[0]), tuple(temp_result[1]))

# Checks if elem is in checked_set, including permutations of the order of elem. Elem is a tuple of the form (X,t) with
# X a tuple of attributes and t a pattern tuple. The elem is assumed to be sorted, as well as each elem in checked_set.
# Example: (("AC", "ZIP"), ("--", "07974")) is assumed to always appear in this order, both in elem and in checked_set.
# The permutation (("ZIP", "AC"), ("07974", "--")) cannot appear, because the attributes are not sorted (notice the
# pattern tuple follows the attribute ordering.
def already_checked(elem, checked_set):
    return elem in checked_set

# Checks whether two elements (a tuple with attributes on the left and a pattern tuple on the right) are the same. The
# attributes do not need to be in the same order, but both elements need to have the same attributes, and the pattern
# tuple needs to have matching elements, following the order of the attributes.
# eg: same elements: (("AC","CC"),("908","--")) and (("CC","AC"),("--","908"))
#     not same elements: (("AC","CC"),("908","--")) and (("CC","AC"),("--","--"))
#     not same elements: (("AC","CC"),("908","--")) and (("CC","AC"),("908","--"))
#     not same elements: (("AC","CC"),("--","--")) and (("CC","ZIP"),("--","--"))
# Remark: this is not used anymore, because elems are always sorted by attribute...
def same_elems(left_elem, right_elem):
    if len(left_elem[0]) != len(right_elem[0]):
        return False
    left_elem_attr_s = set(left_elem[0])
    right_elem_attr_s = set(right_elem[0])
    if left_elem_attr_s != right_elem_attr_s:
        return False
    for attr in left_elem_attr_s:
        i1 = left_elem[0].index(attr)
        i2 = right_elem[0].index(attr)
        val1 = left_elem[1][i1]
        val2 = right_elem[1][i2]
        if val1 != val2:
            return False
    return True

# Computes the partition intersection of the given partitions. The given partitions are sets of frozensets and/or ints,
# which represent the equivalence classes. It is assumed both elems "agree on all but one".
def partition_intersection(left_element, left_partition, right_element, right_partition):
    result = set()
    # This is a heuristic to try to use the partition with less equivalence classes that match its pattern tuple.
    # This also makes sure that if the current element's pattern tuple consists only of wildcards, then the other
    # element's pattern tuple also only consists of wildcards.
    if nb_of_wildcards(left_element[1]) < nb_of_wildcards(right_element[1]):
        curr_elem = left_element
        curr_part = left_partition
        other_elem = right_element
        other_part = right_partition
    else:
        curr_elem = right_element
        curr_part = right_partition
        other_elem = left_element
        other_part = left_partition
    for eq_class in curr_part:
        # If the eq_class is an int, there are two possibilities: 1) the current element's pattern tuple only contains
        # wildcards. In this case, the other element's pattern tuple also only contains wildcards (see above). Because
        # it is a single element, the intersection with any other partition will always result in a single element
        # eq_class. This eq_class matches the pattern tuples of both elements (all wildcards in both cases). The next
        # partitioning will also be according to an all-wildcard pattern tuple, so the eq_class can remain an int.
        # 2) The current element's pattern tuple contains at least one constant. In this case, an int means the eq_class
        # does NOT match the pattern tuple. Intersecting with a partition of any other element will always result in
        # the same eq_class of one element, and it will not match the new element, so the eq_class can remain an int.
        # In both cases, the old eq_class results in a new eq_class of one element, always represented by an int.
        if isinstance(eq_class,int):
            result.add(eq_class)
        # If the eq_class is a frozenset, it matches the pattern tuple of the current element.
        elif isinstance(eq_class, frozenset):
            # The new singleton classes that come from a singleton class in the other partitioning and one element from
            # the current equivalence class are determined first. The if and else clauses figure out if the new
            # singleton classes must be represented by an int, or a frozenset of one int.
            if only_wildcards(other_elem[1]) & (not only_wildcards(curr_elem[1])):
                now_singleton_classes = {frozenset([item]) for item in eq_class if item in other_part}
            else:
                now_singleton_classes = {item for item in eq_class if item in other_part}
            result = result | now_singleton_classes
            # set intersection is used to determine the final equivalence classes.
            split_classes =  {items & eq_class for items in other_part if isinstance(items, frozenset)
                              and len(items & eq_class) != 0}
            result = result | split_classes
    return result

# Returns the amount of wildcards in the given pattern tuple
def nb_of_wildcards(tup):
    return tup.count(WILDCARD_SYMBOL)

# Checks if there are any wildcards present in the given pattern tuple. True means there are wildcards present.
def no_wildcards(tup):
    return nb_of_wildcards(tup) == 0

# Checks if the given pattern tuple consists only of wildcards
def only_wildcards(tup):
    return nb_of_wildcards(tup) == len(tup)

# Determines whether the given partition of some element has a support equal to or greater than the support given. It is
# assumed that the element's pattern tuple (the element itself is not needed, thus it is not in the input) consists of
# constants only. It does this by checking the only matching equivalence class. If the partition is from an
# element with only constants in its pattern tuple, the partitioning should at most contain one frozenset, this is the
# equivalence class that matches the pattern tuple.
def reaches_support_threshold(support, elem_partition):
    matching_eq_classes = {fs for fs in elem_partition if isinstance(fs,frozenset)}
    if len(matching_eq_classes) == 0:
        return False
    elif len(matching_eq_classes.pop()) >= support:
        return True
    else:
        return False

# Checks if all 'sub-elements' of a given element are present in the previous partitions. This is the check for
# Step 4 (b) (iii) in the paper.
def check_all_sub_elems_present(new_elem, previous_partitions):
    for attr in new_elem[0]:
        temp_elem = [list(new_elem[0]), list(new_elem[1])]
        del temp_elem[1][new_elem[0].index(attr)]
        temp_elem[0].remove(attr)
        check_elem = (tuple(temp_elem[0]), tuple(temp_elem[1]))
        if not (check_elem in previous_partitions):
            return False
    return True

# Generates a level that corresponds to the given partition. This assumes the tuple of attributes given as the first
# part of the key of partitions is sorted.
def generate_level_from_partitions(partitions):
    result = defaultdict(list)
    for (attr,tup) in partitions.keys():
        result[attr].append(tup)
    for attr in result:
        # This sorts the list of pattern tuples for each attribute tuple, which the algorithm must keep valid.
        result[attr].sort(key = lambda x: -nb_of_wildcards(x))
    return result

# generates the cplus (dict of dicts) given the current level and previous cplus
def generate_cplus(current_level, previous_cplus):
    result = {}
    for attrs in current_level:
        for tup in current_level[attrs]:
            temp_result_list = []
            for attr in attrs:
                temp_elem = [list(attrs), list(tup)]
                del temp_elem[1][temp_elem[0].index(attr)]
                temp_elem[0].remove(attr)
                elem = (tuple(temp_elem[0]), tuple(temp_elem[1]))
                temp_result_list.append(previous_cplus[elem])
            result[(attrs,tup)] = cplus_intersect_list(temp_result_list)
    return result

# calculates the intersection of a list of "cplus values". These cplus values are similar to the first level, with
# possibly some elements pruned
def cplus_intersect_list(cplus_list):
    return reduce(lambda acc, next_val: cplus_intersect(acc, next_val), cplus_list)

# calculates the intersection of two "cplus values".
def cplus_intersect(cplus1, cplus2):
    result = {}
    both_keys = {key for key in cplus1 if key in cplus2}
    for key in both_keys:
        temp_list = [val for val in cplus1[key] if val in cplus2[key]]
        if len(temp_list) != 0:
            result[key] = temp_list
    return result

# This method returns the new (pruned) CPlus, based on the given CPlus. It also updates the global variable allCFDs.
# The -Step suffix refers to the fact that it is step 2 of the algorithm. The algorithm needs the current and previous
# partitions to efficiently calculate if a CFD holds.
def find_cfds_step(current_level, temp_cplus, current_partitions, previous_partitions, find_ping = False):
    global allCFDs
    global allCFDStrings
    final_cplus = temp_cplus
    for attrs in current_level:
        for tup in current_level[attrs]:
            for attr in attrs:
                # This could be placed before the "for attr in attrs", but this way cplus_X_sp is updated along with
                # final_cplus
                cplus_X_sp = final_cplus[(attrs,tup)]
                if (attr,) in cplus_X_sp and len(cplus_X_sp[(attr,)]) >= 1:
                    # normally, cplus_X_sp[attr] should be a list of one element, and that one element is a tuple of
                    # one element
                    cA = cplus_X_sp[(attr,)][0][0]
                    full_elem = (attrs,tup)
                    temp_tup = list(tup)
                    del temp_tup[attrs.index(attr)]
                    temp_attrs = list(attrs)
                    temp_attrs.remove(attr)
                    reduced_elem = (tuple(temp_attrs), tuple(temp_tup))
                    if partition_length_equal(current_partitions, previous_partitions, full_elem, reduced_elem):
                        found_cfd = CFD(temp_attrs, [attr], temp_tup, [cA])
                        if find_ping:
                            print("CFD found: ", str(found_cfd))
                        allCFDs.append(found_cfd)
                        allCFDStrings.append(str(found_cfd))
                        prune_cplus(final_cplus, attrs, tup, attr, cA)
    remove_empty_cpluses(final_cplus)
    return final_cplus

# Checks if the partition length from curr_part of full_elem is equal to the partition length from prev_part of
# reduced_elem
def partition_length_equal(curr_part, prev_part, full_elem, reduced_elem):
    return len(curr_part[full_elem]) == len(prev_part[reduced_elem])

# prunes the given cplus by the rules defined in step 2 of the CTANE algorithm (see paper)
def prune_cplus(cplus, attrs, tup, attr_A, cA):
    prune_set = set([])
    for elem in cplus:
        if elem[0] != attrs:
            continue
        index = elem[0].index(attr_A)
        if elem[1][index] != cA:
            continue
        if tuples_match(elem[1], tup):
            prune_set.add(elem)
    for elem in prune_set:
        curr_dict = cplus[elem]
        if (attr_A,) in curr_dict:
            del curr_dict[(attr_A,)]
        keys = list(curr_dict.keys())
        for attr in keys:
            # attr should be a tuple of one element
            if not attr[0] in attrs:
                del curr_dict[attr]
    return

# Determines whether the left tuple matches to the right tuple: left_tuple =< right_tuple
def tuples_match(left_tuple, right_tuple):
    if len(left_tuple) != len (right_tuple):
        return False
    else:
        for index in range(len(right_tuple)):
            if right_tuple[index] != WILDCARD_SYMBOL:
                if left_tuple[index] != right_tuple[index]:
                    return False
        return True

# Removes inner dictionaries from the given cplus if they are empty.
def remove_empty_cpluses(cplus):
    keys = list(cplus.keys())
    for key in keys:
        if len(cplus[key]) == 0:
            del cplus[key]
    return

#Step 3 of CTANE (see paper). Returns the pruned current partitions, using the current cplus.
def prune_partitions(current_cplus, current_partitions):
    result_partitions = current_partitions
    for elem in list(current_partitions.keys()):
        if not elem in current_cplus:
            del result_partitions[elem]
    return result_partitions