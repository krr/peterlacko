import pandas
from sklearn import preprocessing
import hdbscan
from collections import deque

# Label encode certain given columns of some given DataFrame. columns_to_encode is assumed to be a set of ints,
# referring to the indices of the columns to label encode. This must be done to every non-numeric column before
# clustering. This method will transform the input data and also return that transformed data.
def label_encode_columns(data, columns_to_encode):
    le = preprocessing.LabelEncoder()
    length = len(data.columns)
    for i in columns_to_encode:
        if i < length:
            data.iloc[:,i] = le.fit_transform(data.iloc[:,i])
    return data

# Same as label_encode_columns, but now all columns will be label encoded.
def label_encode_all_columns(data):
    le = preprocessing.LabelEncoder()
    for col in data:
        data[col] = le.fit_transform(data[col])
    return data

#Example of clustering:
# data = pandas.read_csv(file_name, dtype = object)
# labeled_data = label_encode_all_columns(data.copy())
# algo = hdbscan.HDBSCAN(*(),**{'min_cluster_size':2, 'min_samples':4, 'cluster_selection_method':'leaf','metric':'hamming'})
# algo.fit_predict(labeled_data)
def cluster_data(data,kwds):
    labeled_data = label_encode_all_columns(data.copy())
    algo = hdbscan.HDBSCAN(*(),**kwds)
    labels = algo.fit_predict(labeled_data)
    return labels

# cluster the data and return a pandas DataFrame. Not necessary for the actual CSDTANE run.
def make_clust_df(data,kwds):
    labels = cluster_data(data, kwds)
    clust_dict = {}
    for col in data:
        clust_dict[col] = [set([]) for i in range(max(labels) + 1)]
    clust_data = pandas.DataFrame(clust_dict)
    for i in range(len(labels)):
        if labels[i] != -1:
            for col in data:
                clust_data.iloc[labels[i]][col].add(data.iloc[i][col])
    return clust_data

# Make and return a "clustered dictionary". This is actually different from the clustered database/DataFrame: here we
# just return all the clustered attribute values connected to the attributes, but not connected to any index or tuple
# of the clustered database. Furthermore, if any set is empty or a singleton, it can be dropped because such info is
# useless in the further CSDTANE algorithm. Duplicate sets are also removed for the same reason. Finally, any set that
# goes over the whole domain of an attribute can be dropped as well, since such sets always result in non-minimal or
# trivial CSDs. Note: normally there shouldn't be empty sets, but this is done just to be safe.
def make_clust_dict(data,kwds):
    labels = cluster_data(data,kwds)
    clust_dict = {}
    for col in data:
        clust_dict[col] = [set([]) for i in range(max(labels) + 1)]
    for i in range(len(labels)):
        if labels[i] != -1:
            for col in data:
                clust_dict[col][labels[i]].add(data.iloc[i][col])
    for col in clust_dict:
        dom_size = len(data[col].unique())
        indices_to_del = deque()
        for i in range(len(clust_dict[col])):
            s = clust_dict[col][i]
            if (len(s) == 0) | (len(s) == 1) | (len(s) == dom_size):
                indices_to_del.appendleft(i)
        for i in indices_to_del:
            del clust_dict[col][i]
        new_list = []
        for s in clust_dict[col]:
            if not (s in new_list):
                new_list.append(s)
        clust_dict[col] = new_list
    return clust_dict

# Make a dictionary for the preprocessing step of CSDTANE with partial enumeration. the max_size parameter determines
# the maximum size of the set (so if it is set to 3, all sets of size up to 3 are generated)
def make_enum_dict(data,max_size=0):
    result = {}
    for col in data:
        col_dom = data[col].unique()
        n = max_size
        if n == 0:
            n = len(col_dom)
        ps = powerset(col_dom)
        ps = [x for x in ps if len(x) != 0 and len(x) != 1 and len(x) != len(col_dom) and len(x) <= n]
        result[col] = ps
    return result

# Make a dictionary for the preprocessing step by using the complement sets
def make_compl_dict(data):
    result = {}
    for col in data:
        col_dom = data[col].unique()
        new_list = []
        if len(col_dom) >= 3:
            for elem in col_dom:
                new_set = set(col_dom) - {elem}
                new_list.append(new_set)
        result[col] = new_list
    return result

# Return a list of sets that is a powerset of the given set. May run for a long time if a large set is given.
def powerset(s):
    result = [set([])]
    for elem in s:
        result.extend([x | {elem} for x in result])
    return result