import sys
from csdtaneFunctionsNoMatching import *
from clusteringFunctions import *

def csdtane_main(infile="", k=1, min_cluster_size=3, min_samples=12, to_output=False, find_ping=False,
                 input_cols_str="", do_enum=0):
    allCSDs = []
    allCSDStrings = []

    input_cols_list = []
    if input_cols_str != "":
        input_cols_list = input_cols_str.split(",")

    level_no = 1

    # read the csv file, assuming all tuple elements to be read as 'objects'
    csv_data = read_csv(infile, dtype=object)
    # replace "NaN" values with the empty string
    csv_data = csv_data.fillna("")

    # INITIALIZATION, this technically includes the iteration of l=1

    # find all CFDs with an empty LHS, add their equivalent CSDs to the list of all CSDs. The result of the method
    # returns a list of all columns that are in such an CFD/CSD (here they are called static columns because they always
    # contain 1 value)
    if find_ping:
        print("current level: ", level_no)
    staticCols = find_empty_lhs_cfds_as_csds(csv_data,allCSDs, allCSDStrings, find_ping)
    # The static columns are then dropped
    csv_data = csv_data.drop(columns=staticCols)

    row_count = len(csv_data.index)

    # Cluster the data. This is done after finding static columns, because static columns by definition are all equal,
    # thus using them in clustering wouldn't give that much (if any) extra info, but would use up more resources.
    # The metric is hamming (assume all data is nominal), cluster selection method is leaf, because smaller clusters are
    # preferred. Default min cluster size and min samples are empirically chosen after a few tests (better values might
    # exist).
    if do_enum == 0:
        clustered_dict = make_clust_dict(csv_data, {'min_cluster_size':min_cluster_size, 'min_samples':min_samples,
                                                    'cluster_selection_method':'leaf','metric':'hamming'})
    elif do_enum == -1:
        clustered_dict = make_compl_dict(csv_data)
    else:
        clustered_dict = make_enum_dict(csv_data,do_enum)

    # Generate L1 from the data and clustering. L1 is a dictionary where the keys are a tuple of attributes and the
    # values are a list of set-pattern tuples, eg: L1 == {("CC","AC"): [("--","--"), ("--",{"908"}), ({"01"},"--")],
    # ...}. Also generate the partitions of L1 with the given data and clustering. The partitions are a dictionary, with
    # the key being a tuple of 2 tuples (the first being the attributes, the second being the set-pattern tuple) and the
    #  values are the indices of the csv file's tuples, partitioned into their equivalence classes. For more info, see
    # comments at the top of csdtaneFunctionsParallel.py. Finally, this function also returns the single attribute
    # partitions: these are partitions of the data following a single attribute with a singeton set-pattern tuple.
    # This partition basically remembers which values appear in which tuples and is used to assure k-frequent CSDs.
    (L1, L1Partitions) = generate_level_1_and_partitions(csv_data, clustered_dict, k)
    # generate the (first) C+ set for each element of L1 (C+ represents the RHS candidates)
    CPlus = generate_first_cplus(L1, input_cols_list)
    # generate the next L and partitions of L.
    nextLAndPartitions = generate_next_level_and_partitions(L1,L1Partitions,k)
    # Assign some certain variables with names to make things clear.
    previousPartitions = L1Partitions
    previousCPlus = CPlus
    currentL = nextLAndPartitions[0]
    currentPartitions = nextLAndPartitions[1]
    # While the current level isn't empty, do the following
    while len(currentL) != 0:
        level_no += 1
        if find_ping:
            print("current level: ", level_no)
        # Step 1) generate C+ for the current level, given the previous C+
        tempCPlus = generate_cplus(currentL,previousCPlus)
        # Step 2) for each (X,sp) in currentL, look for valid CSDs. This method returns the new (pruned) CPlus, based on
        # the given CPlus. It also updates 'allCSDs'. The _step suffix refers to the fact that it is step 2 of the
        # algorithm. The algorithm needs the current and previous partitions to efficiently calculate if a CSD holds.
        currentCPlus = find_csds_step(currentL,tempCPlus,currentPartitions,previousPartitions,row_count,allCSDs,
                                      allCSDStrings,find_ping)
        # Step 3) prune away elements of the current level that have an empty C+. In practice, this only actually prunes
        # and returns the partitions.
        previousPartitions, previousL = prune_partitions_and_L(currentCPlus,currentPartitions,currentL)
        # Step 4) generate the next level (and partitions). This step is done in parallel.
        nextLAndPartitions = generate_next_level_and_partitions(previousL,previousPartitions,k)
        previousCPlus = currentCPlus
        currentL = nextLAndPartitions[0]
        currentPartitions = nextLAndPartitions[1]

    # print what was found
    if not find_ping:
        print("List of all CSDs: ")
        for CSDString in allCSDStrings:
            print(CSDString)
    print("Total number of CSDs found: ", len(allCSDs))

    # possibly write to an output file
    if to_output:
        new_name = ".".join(infile.split(".")[:-1])
        if len(new_name) == 0:
            new_name = infile
        new_name = new_name + "_support_" + str(k) + "_CSDs.txt"
        outfile = open(new_name, 'w')
        for csd_str in allCSDStrings:
            outfile.write("%s\n" % csd_str)
        outfile.close()

    return allCSDs

#------------------------------------------------------- START ---------------------------------------------------
if __name__ == '__main__':
    infile = ""
    # if support not given, assume 1
    k = 1
    # default values for clustering
    min_cluster_size = 3
    min_samples = 12
    # default values for special options
    to_output = False
    find_ping = False
    input_cols_str = ""
    do_enum = 0
    if len(sys.argv) > 1:
        infile=str(sys.argv[1])
    if len(sys.argv) > 2:
        k=int(sys.argv[2])
    if len(sys.argv) > 3:
        min_cluster_size=int(sys.argv[3])
    if len(sys.argv) > 4:
        min_samples=int(sys.argv[4])
    if len(sys.argv) > 5:
        to_output=bool(int(sys.argv[5]))
    if len(sys.argv) > 6:
        find_ping=bool(int(sys.argv[6]))
    if len(sys.argv) > 7:
        input_cols_str = str(sys.argv[7])
    if len(sys.argv) > 8:
        do_enum = int(sys.argv[8])
    csdtane_main(infile,k,min_cluster_size,min_samples,to_output,find_ping,input_cols_str,do_enum)